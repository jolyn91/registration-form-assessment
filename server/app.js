var express = require("express");
var bodyParser=require('body-parser');

var app = express();

var NODE_PORT = process.env.PORT || 3000;

//app.use("/bower_components", express.static(__dirname + "/../bower_components"));
app.use(express.static(__dirname + "/../client/"));
//app.use(bodyParser());

app.listen(NODE_PORT, function(){
    console.log("Server running at http://localhost:"+NODE_PORT);
});

app.use(bodyParser.urlencoded({ extended: true }));

app.post('/index.html', function(req, res) {
    res.send('Successful registration. Thank you ' + req.body.name + '! :)');
    console.log('Email: ' + req.body.email)
    console.log('Password: ' + req.body.password);
    console.log('Name: ' + req.body.name);
    console.log('Gender: ' + req.body.gender);
    console.log('Date of birth: ' + req.body.dateOfBirth);
    console.log('Address: ' + req.body.address);
    console.log('Country: ' + req.body.country);
    console.log('Contact no: ' + req.body.contactNo)
});

