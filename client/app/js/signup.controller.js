(function(){
    angular
        .module("SignupApp")
        .controller("SignupCtrl", SignupCtrl);


    SignupCtrl.$inject = [];
    function SignupCtrl(){
        var vm = this;

        vm.userDetails = {
            name: "",
            email: "",
            gender: [],
            dateOfBirth: "",
            address: "",
            country: "",
            contactNo: "",
        };

        vm.userDetailsArray = [];

        vm.signup = signup;


        var today = new Date();
        var minAge = 18;
        vm.minAge = new Date(today.getFullYear() - minAge, today.getMonth(), today.getDate());

        // vm.addData = function () {
        //     vm.userDetailsArray.push(vm.userDetails);
        //     console.info(vm.userDetailsArray);
        //     vm.userDetails ="";
        //
        // };

        function signup(){
            console.info("register click");
            console.info("name: %s", vm.name);
        } // END signUp
    } // End SignUpCtrl
})();